package com.chanu.local.sqlite;

import java.util.List;

import com.chanu.local.domain.HelperComment;
import com.orm.query.Condition;
import com.orm.query.Select;

public class HelperCommentTblHelper {

	public static long saveHelperComment(HelperComment helperComment) {
		if (helperComment != null) {
			return helperComment.save();
		}
		return 0;
	}

	public static List<HelperComment> getCommentsByUserID(Long userId) {
		Select<HelperComment> from = Select.from(HelperComment.class);
		Condition condition = Condition.prop("user_id").eq(userId);
		from.where(condition);
		return from.list();
	}
}
