package com.chanu.local.sqlite;

import java.util.ArrayList;
import java.util.List;

import com.chanu.local.domain.User;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "local";
	private static final String TABLE_USERS = "users";
	private static final String TABLE_HELPER_SERVICE = "helper_service";
	private static final String TABLE_USER_SERVICE = "user_service";
	
	private static final String KEY_ID = "id";
	private static final String KEY_FIRST_NAME = "first_Name";
	private static final String KEY_LAST_NAME = "last_Name";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_PHONE = "phone";
	private static final String KEY_USER_ROLE = "user_role";
	
	private static final String KEY_DATE = "date";
	private static final String KEY_COST = "cost";
	private static final String KEY_SERVICE_TYPE = "service_type";
	private static final String KEY_OWN_EQUIPMENT = "own_equipment";
	
	private static final String KEY_SERVICE_TYPE1 = "service_type1";
	private static final String KEY_SERVICE_TYPE2 = "service_type2";
	private static final String KEY_SIZE = "size";
	private static final String KEY_HELPER_NAME = "helper_name";
	private static final String KEY_PAYMENT_MODE = "payment_mode";
	private static final String KEY_USER_NAME = "user_Name";
	private static final String KEY_RATING = "rating";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		Log.d("Create: ", "Database Created");
		// 3rd argument to be passed is CursorFactory instance
	}

	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create User Table
		Log.d("Table: ", "Users Table Creating");
		String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "(" 
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_FIRST_NAME + " TEXT,"
				+ KEY_LAST_NAME + " TEXT," 
				+ KEY_PASSWORD + " TEXT," 
				+ KEY_EMAIL	+ " TEXT," 
				+ KEY_PHONE + " TEXT,"
				+ KEY_USER_ROLE	+ " TEXT"
				+ ")";
		db.execSQL(CREATE_USERS_TABLE);
		Log.d("Table: ", "Users Table Created");
		
		// Create Helper Service Table
		Log.d("Table: ", "Helper Service Table Creating");
		String CREATE_HELPER_SERVICE_TABLE = "CREATE TABLE " + TABLE_HELPER_SERVICE + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_FIRST_NAME + " TEXT,"
				+ KEY_LAST_NAME + " TEXT," 
				+ KEY_SERVICE_TYPE + " TEXT," 
				+ KEY_DATE	+ " TEXT," 
				+ KEY_OWN_EQUIPMENT + " TEXT,"
				+ KEY_COST + " TEXT,"
				+ KEY_RATING + " TEXT"
				+ ")";
		db.execSQL(CREATE_HELPER_SERVICE_TABLE);
		Log.d("Table: ", "Helper Service Table Created");
		
		// Create User Service Table
				String CREATE_USER_SERVICE_TABLE = "CREATE TABLE " + TABLE_USER_SERVICE + "("
						+ KEY_ID + " INTEGER PRIMARY KEY," 
						+ KEY_USER_NAME + " TEXT,"
						+ KEY_SERVICE_TYPE + " TEXT," 
						+ KEY_SIZE + " TEXT,"
						+ KEY_DATE	+ " TEXT," 
						+ KEY_SERVICE_TYPE1 + " TEXT,"
						+ KEY_SERVICE_TYPE2 + " TEXT,"
						+ KEY_HELPER_NAME + " TEXT,"
						+ KEY_OWN_EQUIPMENT + " TEXT,"
						+ KEY_COST + " TEXT,"
						+ KEY_PAYMENT_MODE + " TEXT"
						+ ")";
				db.execSQL(CREATE_USER_SERVICE_TABLE);
				Log.d("Table: ", "Helper Service Table Created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HELPER_SERVICE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE);

		// Create tables again
		onCreate(db);
	}

	// code to add the new user
	public String addUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FIRST_NAME, user.getFirstName()); // First Name
		values.put(KEY_LAST_NAME, user.getLastName());
		values.put(KEY_PASSWORD, user.getPassword());
		values.put(KEY_EMAIL, user.getEmail());
		values.put(KEY_PHONE, user.getPhone());
		values.put(KEY_USER_ROLE, user.getUserRole());

		// Inserting Row
		db.insert(TABLE_USERS, null, values);
		Log.d("Insert: ", "Values Inserted");
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
		return "success";
	}

	// code to add the new helper service
	public String addHelperService(HelperService helperService) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FIRST_NAME, helperService.getFirstName()); // First Name
		values.put(KEY_LAST_NAME, helperService.getLastName());
		values.put(KEY_SERVICE_TYPE, helperService.getServiceType());
		values.put(KEY_DATE, helperService.getDate());
		values.put(KEY_OWN_EQUIPMENT, helperService.getOwn_equipment());
		values.put(KEY_COST, helperService.getCost());
		values.put(KEY_RATING, helperService.getRating());

		// Inserting Row
		db.insert(TABLE_HELPER_SERVICE, null, values);
		Log.d("Insert: ", "Values Inserted");
		db.close(); // Closing database connection
		return "success";
	}

	
	// code to add the new user service
	public String addUserService(UserService userService) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_USER_NAME, userService.getUserName());
		values.put(KEY_SERVICE_TYPE, userService.getServiceType());
		values.put(KEY_SIZE, userService.getSize());
		values.put(KEY_DATE, userService.getDate());
		values.put(KEY_SERVICE_TYPE1, userService.getServiceType1());
		values.put(KEY_SERVICE_TYPE2, userService.getServiceType2());
		values.put(KEY_HELPER_NAME, userService.getHelperName());
		values.put(KEY_OWN_EQUIPMENT, userService.getOwnEquipment());
		values.put(KEY_COST, userService.getCost());
		values.put(KEY_PAYMENT_MODE, userService.getPaymentMode());

		// Inserting Row
		db.insert(TABLE_USER_SERVICE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
		return "success";
	}

	
	// code to get all users in a list view
	public List<User> getAllusers() {
		List<User> users = new ArrayList<User>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USERS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				User user = new User();
//				user.setId(Integer.parseInt(cursor.getString(0)));
				user.setFirstName(cursor.getString(1));
				user.setLastName(cursor.getString(2));
				user.setPassword(cursor.getString(3));
				user.setEmail(cursor.getString(4));
				user.setPhone(cursor.getString(5));
				user.setUserRole(cursor.getString(6));
				// Adding users to list
				users.add(user);
			} while (cursor.moveToNext());
		}

		// return users list
		return users;
	}
	
	// code to get all helpers in a list view
	public List<HelperService> getAllhelperServices() {
		List<HelperService> helperServices = new ArrayList<HelperService>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_HELPER_SERVICE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				HelperService helperService = new HelperService();
				helperService.setId(Integer.parseInt(cursor.getString(0)));
				helperService.setFirstName(cursor.getString(1));
				helperService.setLastName(cursor.getString(2));
				helperService.setServiceType(cursor.getString(3));
				helperService.setDate(cursor.getString(4));
				helperService.setOwn_equipment(cursor.getString(5));
				helperService.setCost(Integer.parseInt(cursor.getString(6)));
				helperService.setRating(cursor.getString(7));
				// Adding users to list
				helperServices.add(helperService);
			} while (cursor.moveToNext());
		}

		// return users list
		return helperServices;
	}
	
	// code to get all helpers in a list view
		public List<UserService> getAllUserServices() {
			List<UserService> userServices = new ArrayList<UserService>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					UserService userService = new UserService();
					userService.setId(Integer.parseInt(cursor.getString(0)));
					userService.setUserName(cursor.getString(1));
					userService.setServiceType(cursor.getString(2));
					userService.setSize(cursor.getString(3));
					userService.setDate(cursor.getString(4));
					userService.setServiceType1(cursor.getString(5));
					userService.setServiceType2(cursor.getString(6));
					userService.setHelperName(cursor.getString(7));
					userService.setOwnEquipment(cursor.getString(8));
					userService.setCost(Integer.parseInt(cursor.getString(9)));
					userService.setPaymentMode(cursor.getString(10));
					// Adding users to list
					userServices.add(userService);
				} while (cursor.moveToNext());
			}

			// return users list
			return userServices;
		}


}
