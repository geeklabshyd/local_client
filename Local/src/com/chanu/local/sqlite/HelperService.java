package com.chanu.local.sqlite;

public class HelperService {

	private int id;
	private String firstName;
	private String lastName;
	private String serviceType;
	private String date;
	private int cost;
	private String own_equipment;
	private String rating;

	public HelperService() {
	}

	public HelperService(int id, String serviceType, String date) {
		this.id = id;
		this.serviceType = serviceType;
		this.date = date;
	}

	public HelperService(String serviceType, String date) {
		this.serviceType = serviceType;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getOwn_equipment() {
		return own_equipment;
	}

	public void setOwn_equipment(String own_equipment) {
		this.own_equipment = own_equipment;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

}
