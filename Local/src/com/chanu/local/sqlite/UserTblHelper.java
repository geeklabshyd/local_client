package com.chanu.local.sqlite;

import java.util.List;

import com.chanu.local.domain.User;
import com.orm.query.Condition;
import com.orm.query.Select;

public class UserTblHelper {

	public static User saveUser(User user) {
		
		if (user != null) {
			user.save();
			return user;
		} 
		return null;
	}
	
	public static List<User> getAllUsers() {
		return User.listAll(User.class);
	}
	
	public static User getUserByUserNameAndPassword(String userName, String password) {

		Select<User> from = Select.from(User.class);
		Condition eqUserName = Condition.prop("first_name").eq(userName);
		Condition eqUserPassword = Condition.prop("password").eq(password);
		from.where(eqUserName, eqUserPassword);
		return from.first();
	}
}
