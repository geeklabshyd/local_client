package com.chanu.local.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.chanu.local.R;
import com.chanu.local.adapter.CommentsAdaptor;
import com.chanu.local.domain.HelperComment;
import com.chanu.local.sqlite.HelperCommentTblHelper;

public class HelperCommentActivity extends Activity {

	private Long userId;
	private CommentsAdaptor commentsAdaptor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.helper_commets);

		findViewById(R.id.comment_here).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), HelperCommentRateActivity.class));
			}
		});

		Intent intent = getIntent();
		userId = intent.getLongExtra("id", 0);
		
		List<HelperComment> commentsByUserID = HelperCommentTblHelper.getCommentsByUserID(userId);

		for (HelperComment helperComment : commentsByUserID) {
			TextView t = new TextView(this);
			t = (TextView) findViewById(R.id.comment_textView);
			t.setText(helperComment.toString());
		}

		commentsAdaptor = new CommentsAdaptor();
		ListView listView = (ListView) findViewById(android.R.id.list);
		listView.setAdapter(commentsAdaptor);

	}
}
