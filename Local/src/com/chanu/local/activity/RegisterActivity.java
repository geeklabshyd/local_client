package com.chanu.local.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.chanu.local.R;
import com.chanu.local.domain.User;
import com.chanu.local.preferences.AuthPreferences;
import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.UserTblHelper;

public class RegisterActivity extends Activity {

	private EditText firstName, lastName, password, email, phone;
	private Spinner userType;
	private String userType_selected;
	private String responseStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		final DatabaseHandler db = new DatabaseHandler(this);

		userType = (Spinner) findViewById(R.id.userType);
		firstName = (EditText) findViewById(R.id.firstName);
		lastName = (EditText) findViewById(R.id.lastName);
		password = (EditText) findViewById(R.id.password);
		email = (EditText) findViewById(R.id.email);
		phone = (EditText) findViewById(R.id.phone);

		// User Type View
		ArrayAdapter<CharSequence> userType_adapter = ArrayAdapter
				.createFromResource(this, R.array.userTypes,
						android.R.layout.simple_spinner_item);
		userType_adapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		userType.setAdapter(userType_adapter);

		findViewById(R.id.register).setOnClickListener(new OnClickListener() {

			private AuthPreferences authPreferences;

			@Override
			public void onClick(View v) {
				userType_selected = ((Spinner) findViewById(R.id.userType))
						.getSelectedItem().toString();
				// Inserting Contacts
				if (userType_selected.trim().equals("User Type")) {
					Toast.makeText(getApplicationContext(),
							"User Type Required", Toast.LENGTH_LONG).show();
				}
				if (firstName.getText().toString().trim().equals("")) {
					firstName.setHint("First Name is Required");
					firstName.setError("");
				}
				if (password.getText().toString().trim().equals("")) {
					password.setHint("Password is Required");
					password.setError("");
				} else {
					Log.d("Insert: ", "Inserting ..");
					User user = new User();
					user.setFirstName(firstName.getText().toString());
					user.setLastName(lastName.getText().toString());
					user.setPassword(password.getText().toString());
					user.setEmail(email.getText().toString());
					user.setPhone(phone.getText().toString());
					
					if (userType_selected.trim().equals("User")) {
						user.setUserRole("User");
						User saveUser = UserTblHelper.saveUser(user);
						
						if (saveUser != null && saveUser.getUserRole().equals("User")) {
							startActivity(new Intent(RegisterActivity.this,	UserDashboardActivity.class));
							authPreferences = new AuthPreferences(RegisterActivity.this);
							authPreferences.setSignInStatus(true);
							authPreferences.setUserRole(saveUser.getUserRole());
							authPreferences.setUserId(saveUser.getId());
							RegisterActivity.this.finish();
						}
					} else if (userType_selected.trim().equals("Helper")){
						user.setUserRole("Helper");
						User saveUser = UserTblHelper.saveUser(user);
						if (saveUser != null && saveUser.getUserRole().equals("Helper")) {
							startActivity(new Intent(RegisterActivity.this,	HelperDashboardActivity.class));
							authPreferences = new AuthPreferences(RegisterActivity.this);
							authPreferences.setSignInStatus(true);
							authPreferences.setUserRole(saveUser.getUserRole());
							authPreferences.setUserId(saveUser.getId());
							RegisterActivity.this.finish();
						}
					}else{
						Toast.makeText(getApplicationContext(), "Selected User Type Wrong!!", Toast.LENGTH_LONG).show();
					}
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}
}
