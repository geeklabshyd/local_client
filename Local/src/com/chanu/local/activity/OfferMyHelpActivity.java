package com.chanu.local.activity;

import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.chanu.local.R;
import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.HelperService;

public class OfferMyHelpActivity extends Activity {

	private Spinner serviceType;
	private TextView dateTextView;
	private EditText cost;
	private ToggleButton ownEquipment;
	private DatePicker datePicker;
	private Calendar calendar;
	private int year, month, day;
	private String serviceTypeSelected;
	private DatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offer_my_help);

		db = new DatabaseHandler(this);

		serviceType = (Spinner) findViewById(R.id.serviceTypeSpinner);
		dateTextView = (TextView) findViewById(R.id.dateTextView);
		ownEquipment = (ToggleButton) findViewById(R.id.ownEquipment);
		cost = (EditText) findViewById(R.id.cost);

		// Service Type
		ArrayAdapter<CharSequence> serviceType_adapter = ArrayAdapter
				.createFromResource(this, R.array.serviceTypes,
						android.R.layout.simple_spinner_item);
		serviceType_adapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		serviceType.setAdapter(serviceType_adapter);

		// Date View
		calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DATE);
		showDate(year, month+1, day);
		
		// Reading all contacts  
        Log.d("Reading: ", "Reading all contacts..");  
        List<HelperService> helperServices = db.getAllhelperServices();         
   
        for (HelperService helperService : helperServices) {  
         String log = "Id: "+helperService.getId()
        		 + " ,First Name: " + helperService.getFirstName() 
        		 + " ,Last Name: " + helperService.getLastName()
        		 + " ,Service Type: " + helperService.getServiceType()
        		 + " ,Date: " + helperService.getDate()
        		 + " ,Own equipment: " + helperService.getOwn_equipment()
        		 + " ,Cost: " + helperService.getCost();  
        Log.d("Helpers: ", log);  
    }  
	}

	@SuppressWarnings("deprecation")
	public void setDate(View view) {
		showDialog(999);
		Toast.makeText(getApplicationContext(), "Change date",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 999) {
			return new DatePickerDialog(this, myDateListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
			// arg1 = year
			// arg2 = month
			// arg3 = day
			showDate(arg1, arg2+1, arg3);
		}
	};

	private void showDate(int year, int month, int day) {
		dateTextView.setText(new StringBuilder().append(day).append("-")
				.append(month).append("-").append(year));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getTitle().equals("save")) {

			serviceTypeSelected = ((Spinner) findViewById(R.id.serviceTypeSpinner))
					.getSelectedItem().toString();

			if (serviceTypeSelected.trim().equals("Select Service Type")) {
				Toast.makeText(getApplicationContext(),
						"Service Type Required", Toast.LENGTH_LONG).show();
			}
			if (cost.getText().toString().trim().equals("")) {
				cost.setHint("Cost is Required");
				cost.setError("");
			} else {
				Log.d("Insert: ", "Inserting ..");
				HelperService helperService = new HelperService();
				helperService.setFirstName("Helper");
				helperService.setLastName("a");
				helperService.setServiceType(serviceTypeSelected);
				helperService.setDate(dateTextView.getText().toString());
				helperService.setOwn_equipment(ownEquipment.getText()
						.toString());
				helperService.setCost(Integer.parseInt(cost.getText().toString()));
				helperService.setRating("Not Available");

				if (db.addHelperService(helperService).trim().equals("success")) {
					Toast.makeText(getApplicationContext(),
							"You Help Service Saved successfully", Toast.LENGTH_LONG)
							.show();
					finish();
				} else {
					Toast.makeText(getApplicationContext(),
							"Failed to save your help", Toast.LENGTH_LONG)
							.show();
				}
			}

		}
		return super.onOptionsItemSelected(item);
	}
}
