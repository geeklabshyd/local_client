package com.chanu.local.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.chanu.local.preferences.AuthPreferences;
import com.chanu.local.util.AdRequestUtil;
import com.chanu.local.R;
import com.google.android.gms.ads.AdView;

public class HelperDashboardActivity extends Activity {
	
	private LinearLayout offerMyHelp;
	private AdView adView;
	private AuthPreferences authPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_helper_dashboard);
		adView = (AdView) this.findViewById(R.id.adViewMyAccount);
		adView.loadAd(AdRequestUtil.getAdRequest());
		
		offerMyHelp = (LinearLayout) findViewById(R.id.offerMyHelp);
		offerMyHelp.setOnClickListener(new LinearLayout.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent( getApplicationContext(), OfferMyHelpActivity.class));
			}
		});
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.helpers_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.signout) {
			authPreferences = new AuthPreferences(this);
			authPreferences.setUserRole("");
			authPreferences.setSignInStatus(false);
			authPreferences.setUserId(0);
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
	        startActivity(intent);
	        finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
}
