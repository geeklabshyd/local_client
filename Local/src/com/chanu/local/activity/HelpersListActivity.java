package com.chanu.local.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.chanu.local.R;
import com.chanu.local.adapter.HelpersListAdapter;
import com.chanu.local.preferences.AuthPreferences;
import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.HelperService;

public class HelpersListActivity extends ListActivity {
	private HelpersListAdapter helpersListAdapter;
	private String date;
	private String serviceType;
	private AuthPreferences authPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_helpers_list);
		Intent intent = getIntent();
		date = intent.getStringExtra("date");
		serviceType = intent.getStringExtra("serviceType");
//		findViewById(android.R.id.list);
		List<HelperService> helpersList = getHelpersList();
		helpersListAdapter = new HelpersListAdapter(this, helpersList);
		setListAdapter(helpersListAdapter);
		
		onItemClick();
		

		    /*    OnItemClickListener myListViewClicked = new OnItemClickListener() {

		            @Override
		            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		                Toast.makeText(getApplicationContext(), "Clicked at positon = " + position, Toast.LENGTH_SHORT).show();

		            }
		        };
		getListView().setOnItemClickListener(myListViewClicked) ;*/
	}

	private void onItemClick() {
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				HelperService helperService = helpersListAdapter.getHelperServices().get(position);
				int userId = helperService.getId();
				
				
				Intent intent = new Intent(HelpersListActivity.this, HelperCommentActivity.class);
				intent.putExtra("id", userId);
				startActivity(intent);
			}
		});
		
	}

	private List<HelperService>  getHelpersList() {
		
		List<HelperService> helperServices = getHelpers();
		List<HelperService> helpersByServiceDate = new ArrayList<HelperService>();
		for (HelperService helpers : helperServices) {
			if (helpers.getServiceType().toString().trim().equals(serviceType) && helpers.getDate().toString().trim().equals(date)) {
				helpersByServiceDate.add(helpers);

			}
		}
		return helpersByServiceDate;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.helpers_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.signout) {
			authPreferences = new AuthPreferences(this);
			authPreferences.setUserRole("");
			authPreferences.setSignInStatus(false);
			authPreferences.setUserId(0);
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
	        startActivity(intent);
	        finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public List<HelperService> getHelpers() {
		DatabaseHandler db = new DatabaseHandler(this);
		List<HelperService> helperServices = db.getAllhelperServices();
		return helperServices;
	}
}
