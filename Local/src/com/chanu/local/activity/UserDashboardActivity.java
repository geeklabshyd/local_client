package com.chanu.local.activity;

import java.io.Serializable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.chanu.local.adapter.UserDashboardAdapter;
import com.chanu.local.preferences.AuthPreferences;
import com.chanu.local.util.AdRequestUtil;
import com.chanu.local.R;
import com.google.android.gms.ads.AdView;

public class UserDashboardActivity extends Activity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private UserDashboardAdapter userDashboardAdapter;
	private AdView adView;
	private AuthPreferences authPreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_dashboard);
		
		// to get adds
		adView = (AdView) this.findViewById(R.id.adViewMyAccount);
		adView.loadAd(AdRequestUtil.getAdRequest());
		
		userDashboardAdapter = new UserDashboardAdapter(this);
		ListView listView = (ListView) findViewById(R.id.dashboardlist);
		listView.setAdapter(userDashboardAdapter);
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				Intent intent = new Intent(UserDashboardActivity.this, UserServiceActivity.class);
				intent.putExtra("position", position);
				startActivity(intent);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.helpers_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.signout) {
			authPreferences = new AuthPreferences(this);
			authPreferences.setUserRole("");
			authPreferences.setSignInStatus(false);
			authPreferences.setUserId(0);
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
	        startActivity(intent);
	        finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
}
