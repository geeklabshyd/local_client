package com.chanu.local.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.chanu.local.R;
import com.chanu.local.domain.User;
import com.chanu.local.preferences.AuthPreferences;
import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.UserTblHelper;
import com.chanu.local.util.AdRequestUtil;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity {

	private AuthPreferences authPreferences;
	private static final String TAG = "MainActivity";
	private EditText userNameEditText, passwordEditText;
	private String userName, password;
	private String status = "ok";
	private DatabaseHandler db;
	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// to get adds
		adView = (AdView) this.findViewById(R.id.adViewMyAccount);
		adView.loadAd(AdRequestUtil.getAdRequest());
		authPreferences = new AuthPreferences(getApplicationContext());

		userNameEditText = (EditText) findViewById(R.id.userName_EditText);
		passwordEditText = (EditText) findViewById(R.id.password_EditText);

		db = new DatabaseHandler(this);
		
		long userId = authPreferences.getUserId();
		boolean userSignedIn = authPreferences.isUserSignedIn();
		if (userSignedIn && userId > 0) {
			String userRole = authPreferences.getUserRole();
			if (userRole.equals("User")) {
				startActivity(new Intent(this, UserDashboardActivity.class));
				finish();
			}
			
			if (userRole.equals("Helper")) {
				startActivity(new Intent(this, HelperDashboardActivity.class));
				finish();
			}
		} else {
		findViewById(R.id.login_button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				userName = userNameEditText.getText().toString();
				password = passwordEditText.getText().toString();

				if (userNameEditText.getText().toString().trim().equals("")) {
					userNameEditText.setError("User Name Required");
				}
				if (passwordEditText.getText().toString().trim().equals("")) {
					passwordEditText.setError("Password Required");
				} else {
					validateUser(userName, password);
				}

			}

			private void validateUser(String userName, String password) {
				

				User user = UserTblHelper.getUserByUserNameAndPassword(userName, password);
				if (user != null && user.getUserRole().equals("User")) {
					startActivity(new Intent(getApplicationContext(), UserDashboardActivity.class));
					authPreferences = new AuthPreferences(MainActivity.this);
					authPreferences.setSignInStatus(true);
					authPreferences.setUserRole(user.getUserRole());
					authPreferences.setUserId(user.getUId());
					MainActivity.this.finish();
				} else if(user != null && user.getUserRole().equals("Helper")){
					startActivity(new Intent(getApplicationContext(), HelperDashboardActivity.class));
					authPreferences = new AuthPreferences(MainActivity.this);
					authPreferences.setSignInStatus(true);
					authPreferences.setUserRole(user.getUserRole());
					authPreferences.setUserId(user.getUId());
					MainActivity.this.finish();
				} else {
					Toast.makeText(MainActivity.this, "User Name or Password wrong!!!.", Toast.LENGTH_LONG).show();
				}
				
				/*

				// Reading all users
				Log.d("Reading: ", "Reading all users..");
				List<User> users = User.listAll(User.class);
				for (User user : users) {
					if (userName.trim().equals(user.getFirstName()) && password.trim().equals(user.getPassword())) {
						if (user.getUserRole().trim().equals("User")) {
							startActivity(new Intent(getApplicationContext(), UserDashboardActivity.class));
							break;
						} else if (user.getUserRole().trim().equals("Helper")) {
							startActivity(new Intent(getApplicationContext(), HelperDashboardActivity.class));
							break;
						} else {
							status = "wrong";
						}

					} else {
					}
				}
				if (status.trim().equals("wrong")) {
					Toast.makeText(MainActivity.this, "User Name or Password wrong!!!.", Toast.LENGTH_LONG).show();
				}
			*/}

		});
		}

		findViewById(R.id.register_button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
	
}
