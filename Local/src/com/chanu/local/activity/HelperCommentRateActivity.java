package com.chanu.local.activity;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.chanu.local.R;
import com.chanu.local.domain.HelperComment;
import com.chanu.local.sqlite.HelperCommentTblHelper;

public class HelperCommentRateActivity extends Activity {

	private static final int CAMERA_REQUEST = 1888;
	private ImageView imageView;
	private Spinner spRating;
	private EditText etvComment;
	private Button btnSubmit;
	private byte[] imageByteArray;
	private String comment;
	private String rating;
	private HelperComment helperComment = new HelperComment();
	private Long userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_helper_comment);
		
		imageView = (ImageView) this.findViewById(R.id.imageView1);
		etvComment = (EditText) this.findViewById(R.id.editText1);
		spRating = (Spinner) this.findViewById(R.id.spRating);
		btnSubmit = (Button) this.findViewById(R.id.commentButton);

		comment = etvComment.getText().toString();
		rating = spRating.getSelectedItem().toString();
		

		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, CAMERA_REQUEST);
			}
		});

		onclickonSubmit();
	}

	private void onclickonSubmit() {
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				helperComment.setComment(comment);
				helperComment.setRating(Integer.parseInt(rating));
				helperComment.setUserId(userId);
				if (!TextUtils.isEmpty(comment)) {
					if (imageByteArray != null && imageByteArray.length > 0) {
						helperComment.setBitMap(imageByteArray);
					}

					HelperCommentTblHelper.saveHelperComment(helperComment);
					setEmptyTextViews();
				} else {
					etvComment.setError("please enter comment..!");
				}
			}
		});

	}

	private void setEmptyTextViews() {

		etvComment.setText("");
		imageView.setImageResource(R.drawable.ic_launcher);
		spRating.setSelection(0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_REQUEST) {
			Bitmap photo = (Bitmap) data.getExtras().get("data");
			imageView.setImageBitmap(photo);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
			imageByteArray = stream.toByteArray();
		}
	}
}
