package com.chanu.local.activity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.chanu.local.adapter.HelpersListAdapter;
import com.chanu.local.adapter.UserDashboardAdapter;
import com.chanu.local.domain.User;
import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.HelperService;
import com.chanu.local.sqlite.UserService;
import com.chanu.local.R;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

public class UserServiceActivity extends Activity implements Serializable {

	private Intent intent;
	private UserDashboardAdapter userDashboardAdapter;
	private TextView listItemName, serviceType1Name, serviceType2Name, dateTextview, helpersCount, helperTextView, availableTextView;
	private ImageView listImage;
	private Button sizeSmall, sizemedium, sizeLarge, rating, byPrice;
	private ToggleButton serviceType1, serviceType2, ownequipment;
	private String size = "small";
	private DatePicker datePicker;
	private Calendar calendar;
	private int year, month, day;
	private ListView helperslist;
	private HelpersListAdapter helpersListAdapter;
	private DatabaseHandler db;
	
	private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    private static final String CONFIG_CLIENT_ID = "AXWsj64jIuY2BuQJDV3hOjIXAu2xb4WeMq0bnG64YQN_AGax4JAmCQeEEz6CR_jGvzV_p3sXHFINwfTm";

    private static final int REQUEST_PAYPAL_PAYMENT = 1;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Android Hub 4 You")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_service);

		Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        
		db = new DatabaseHandler(this);

		findViewById(R.id.payment).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(1),"USD", "androidhub4you.com",
			            PayPalPayment.PAYMENT_INTENT_SALE);

			        Intent intent = new Intent(UserServiceActivity.this, PaymentActivity.class);

			        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

			        startActivityForResult(intent, REQUEST_PAYPAL_PAYMENT);
			}
		});

		// Change Service types based on your requirements
		String serviceType1Names[] = { "Remove Weeds", "Only Water Servicing", "Small Plants", "service type 1" };
		String serviceType2Names[] = { "Water the Lawn", "Complete with engine servicing", "Big Trees", "service type 2" };
		
		intent = getIntent();
		int position = (int) intent.getDoubleExtra("position",0);
		userDashboardAdapter = new UserDashboardAdapter(this);

		listItemName = (TextView) findViewById(R.id.listItemName);
		listImage = (ImageView) findViewById(R.id.listImage);
		sizeSmall = (Button) findViewById(R.id.serviceSize_small);
		sizemedium = (Button) findViewById(R.id.serviceSize_medium);
		sizeLarge = (Button) findViewById(R.id.serviceSize_large);
		serviceType1Name = (TextView) findViewById(R.id.serviceType1Name);
		serviceType2Name = (TextView) findViewById(R.id.serviceType2Name);
		serviceType1 = (ToggleButton) findViewById(R.id.serviceType1);
		serviceType2 = (ToggleButton) findViewById(R.id.serviceType2);
		dateTextview = (TextView) findViewById(R.id.dateTextview);
		ownequipment = (ToggleButton) findViewById(R.id.ownequipment);
		helpersCount = (TextView) findViewById(R.id.helpersCount);
		helperTextView = (TextView) findViewById(R.id.helpersTextView);
		availableTextView = (TextView) findViewById(R.id.availableTextView);
		rating = (Button) findViewById(R.id.rating);
		byPrice = (Button) findViewById(R.id.byPrice);
		helperslist = (ListView) findViewById(R.id.helperslist);

		listItemName.setText(userDashboardAdapter.getString(position));
		listImage.setImageResource((int) userDashboardAdapter.getItemId(position));
		sizeSmall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sizeSmall.setBackgroundColor(Color.BLUE);
				sizemedium.setBackgroundColor(Color.GRAY);
				sizeLarge.setBackgroundColor(Color.GRAY);
				size = "small";

			}
		});
		sizemedium.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sizeSmall.setBackgroundColor(Color.GRAY);
				sizemedium.setBackgroundColor(Color.BLUE);
				sizeLarge.setBackgroundColor(Color.GRAY);
				size = "medium";
			}
		});
		sizeLarge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sizeSmall.setBackgroundColor(Color.GRAY);
				sizemedium.setBackgroundColor(Color.GRAY);
				sizeLarge.setBackgroundColor(Color.BLUE);
				size = "large";
			}
		});

		serviceType1Name.setText(serviceType1Names[position]);
		serviceType2Name.setText(serviceType2Names[position]);
		// Date View
		calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DATE);
		showDate(year, month + 1, day);

		List<HelperService> helperServices = new ArrayList<HelperService>();
		helpersListAdapter = new HelpersListAdapter(UserServiceActivity.this, helperServices);
		helperslist.setAdapter(helpersListAdapter);
		
		helperslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				Intent intent = new Intent(UserServiceActivity.this, HelperCommentActivity.class);
				intent.putExtra("position", position);
				startActivity(intent);
			}
		});

		helpersCount.setVisibility(View.GONE);
		helperTextView.setVisibility(View.GONE);
		availableTextView.setVisibility(View.GONE);
		rating.setVisibility(View.GONE);
		byPrice.setVisibility(View.GONE);

	}

	@SuppressWarnings("deprecation")
	public void setDate(View view) {
		showDialog(999);
		Toast.makeText(getApplicationContext(), "Change date", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 999) {
			return new DatePickerDialog(this, myDateListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
			// arg1 = year
			// arg2 = month
			// arg3 = day
			showDate(arg1, arg2 + 1, arg3);
			
//			call helpers list activity
			Intent intent = new Intent(UserServiceActivity.this, HelpersListActivity.class);
			intent.putExtra("serviceType", listItemName.getText().toString());
			intent.putExtra("date", dateTextview.getText().toString());
			startActivity(intent);
			getHelperList();
		}
	};

	private void showDate(int year, int month, int day) {
		dateTextview.setText(new StringBuilder().append(day).append("-").append(month).append("-").append(year));

	}

	public void getHelperList() {
		List<HelperService> helperServices = getHelpers();
		List<HelperService> helpersByServiceDate = new ArrayList<HelperService>();
		String servicetype = listItemName.getText().toString();
		String date = dateTextview.getText().toString();
		for (HelperService helpers : helperServices) {
			if (helpers.getServiceType().toString().trim().equals(servicetype) && helpers.getDate().toString().trim().equals(date)) {
				helpersByServiceDate.add(helpers);

			}
		}

		helpersListAdapter.setHelperServices(helpersByServiceDate);
		helpersListAdapter.notifyDataSetChanged();
		helpersCount.setVisibility(View.VISIBLE);
		helperTextView.setVisibility(View.VISIBLE);
		availableTextView.setVisibility(View.VISIBLE);
		rating.setVisibility(View.VISIBLE);
		byPrice.setVisibility(View.VISIBLE);

		helpersCount.setText("" + helpersByServiceDate.size());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("save").setIcon(R.drawable.save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getTitle().equals("save")) {

			Log.d("Insert: ", "Inserting ..");
			UserService userService = new UserService();
			HelperService helperService = helpersListAdapter.getSelectedHelper();

			userService.setUserName("User");// need change
			userService.setServiceType(listItemName.getText().toString());
			userService.setDate(dateTextview.getText().toString());
			userService.setServiceType1(serviceType1.getText().toString());
			userService.setServiceType2(serviceType2.getText().toString());
			userService.setSize(size);
			userService.setHelperName("" + helperService.getFirstName() + helperService.getLastName());
			userService.setOwnEquipment(ownequipment.getText().toString());
			userService.setCost(helperService.getCost());
			userService.setPaymentMode("By Cash");// Need to change

			if (db.addUserService(userService).trim().equals("success")) {

				/*
				 * // Reading all User Services.. Log.d("Reading: ",
				 * "Reading all User Services.."); List<UserService>
				 * UserServices = db.getAllUserServices();
				 * 
				 * for (UserService sn : UserServices) { String log =
				 * "Id: "+sn.getId() +" ,User Name: " + sn.getUserName() +
				 * " ,Service Type: " + sn.getServiceType() + " ,Size: " +
				 * sn.getSize() + " ,Date: " + sn.getDate() +
				 * " ,Service Type1: " + sn.getServiceType1() +
				 * " ,Service Type2: " + sn.getServiceType2() +
				 * " ,Helper Name: " + sn.getHelperName() + " ,Own equipment: "
				 * + sn.getOwnEquipment() + " ,cost : " + sn.getCost() +
				 * " ,payment mode: " + sn.getPaymentMode(); // Writing Contacts
				 * to log Log.d("User Services: ", log); }
				 */
				Toast.makeText(getApplicationContext(), "Your Needed Help Service Saved successfully", Toast.LENGTH_LONG).show();
				finish();
			} else {
				Toast.makeText(getApplicationContext(), "Failed to save your needed help service", Toast.LENGTH_LONG).show();
			}
		}
		return super.onOptionsItemSelected(item);

	}

	public List<HelperService> getHelpers() {
		List<HelperService> helperServices = db.getAllhelperServices();
		return helperServices;
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PAYPAL_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                    	System.out.println("Responseeee"+confirm);
                        Log.i("paymentExample", confirm.toJSONObject().toString());

                      
                        JSONObject jsonObj=new JSONObject(confirm.toJSONObject().toString());
                        
                        String paymentId=jsonObj.getJSONObject("response").getString("id");
                        System.out.println("payment id:-=="+paymentId);
                        Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();  

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
            }
        } 
        
         
  }

}
