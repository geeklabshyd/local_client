package com.chanu.local.domain;

import com.orm.SugarRecord;

public class User extends SugarRecord<User> {

	private int uid;
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private String phone;
	private String userRole;

	/*
	 * public User() { }
	 * 
	 * public User(int id, String firstName, String password) { this.uid = id;
	 * this.firstName = firstName; this.password = password; }
	 * 
	 * public User(String userName, String password) { this.firstName =
	 * userName; this.password = password; }
	 */

	public int getUId() {
		return uid;
	}

	public void setUId(int id) {
		this.uid = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

}
