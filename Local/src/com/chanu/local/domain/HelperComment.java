package com.chanu.local.domain;

import com.orm.SugarRecord;

public class HelperComment extends SugarRecord<HelperComment> {

	private String comment;
	private int rating;
	private byte[] bitMap;
	private long userId;
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getUserId() {
		return userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public byte[] getBitMap() {
		return bitMap;
	}

	public void setBitMap(byte[] bitMap) {
		this.bitMap = bitMap;
	}
}
