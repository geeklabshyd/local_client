package com.chanu.local.dto;

public class UserServiceDto {
	private int id;
	private String userName;
	private String serviceType;
	private String date;
	private String serviceType1;
	private String serviceType2;
	private String size;
	private String helperName;
	private String helperOwnEquipment;
	private int cost;
	private String paymentMode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getServiceType1() {
		return serviceType1;
	}

	public void setServiceType1(String serviceType1) {
		this.serviceType1 = serviceType1;
	}

	public String getServiceType2() {
		return serviceType2;
	}

	public void setServiceType2(String serviceType2) {
		this.serviceType2 = serviceType2;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getHelperName() {
		return helperName;
	}

	public void setHelperName(String helperName) {
		this.helperName = helperName;
	}

	public String getHelperOwnEquipment() {
		return helperOwnEquipment;
	}

	public void setHelperOwnEquipment(String helperOwnEquipment) {
		this.helperOwnEquipment = helperOwnEquipment;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

}
