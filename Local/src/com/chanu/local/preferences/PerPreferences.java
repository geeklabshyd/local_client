package com.chanu.local.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PerPreferences {

	private static String DIALED_CALL_STATUS = "dialled";
	private static String MISSED_CALL_STATUS = "missed";
	private static String RECEIVED_CALL_STATUS = "received";
	private static String MESSAGE_STATUS = "message";
	private static String ALL_PERSONALIZES = "all";
	private static final String ID = "0";
	private static final String IS_WIFI_ONLY = "wifi_only";
	private static final String CONTACTS = "contacts";
	private static final String IS_WIFI_OR_DATA = "wifi_or_data";
	
	private SharedPreferences preferences;
	
	public PerPreferences(Context context){
		preferences = context.getSharedPreferences("callhistory_personalized_preferences", Context.MODE_PRIVATE);
	}
	
	public void setId(long uId) {
		Editor editor = preferences.edit();
		editor.putLong(ID, uId);
		editor.commit();
	}

	public long getId() {
		return preferences.getLong(ID, 0);
	}
	
	public  void setDialedCallStatus(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(DIALED_CALL_STATUS, status);
		editor.commit();
	}
	public  boolean isDialedCallStatus(){
		return preferences.getBoolean(DIALED_CALL_STATUS, false);
	}
	public  void setReceivedCallStatus(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(RECEIVED_CALL_STATUS, status);
		editor.commit();
	}
	public  boolean isReceivedCallStatus(){
		return preferences.getBoolean(RECEIVED_CALL_STATUS, false);
	}
	public  void setMissedCallStatus(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(MISSED_CALL_STATUS, status);
		editor.commit();
	}
	public  boolean isMissedCallStatus(){
		return preferences.getBoolean(MISSED_CALL_STATUS, false);
	}
	public  void setMessageStatus(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(MESSAGE_STATUS, status);
		editor.commit();
	}
	public  boolean isMessageStatus(){
		return preferences.getBoolean(MESSAGE_STATUS, false);
	}
	
	public  void setAllPersonizeStatus(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(ALL_PERSONALIZES, status);
		editor.commit();
	}
	
	public  boolean isAllPersonalized(){
		return preferences.getBoolean(ALL_PERSONALIZES, true);
	}
	
	public  void setSyncWifiOnly(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(IS_WIFI_ONLY, status);
		editor.commit();
	}

	public boolean isSyncOnlyWifi() {
		return preferences.getBoolean(IS_WIFI_ONLY, true);
	}
	
	public  void setSyncWifiOrData(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(IS_WIFI_OR_DATA, status);
		editor.commit();
	}

	public boolean isSyncWifiOrData() {
		return preferences.getBoolean(IS_WIFI_OR_DATA, false);
	}

	public  void setContactsEnabled(boolean status) {
		Editor editor = preferences.edit();
		editor.putBoolean(CONTACTS, status);
		editor.commit();
	}
	public boolean isContactsEnabled() {
		return preferences.getBoolean(CONTACTS, false);
	}
	
	public void clearPreferences() {
		Editor editor = preferences.edit();
		
		editor.remove(ALL_PERSONALIZES);
		editor.remove(DIALED_CALL_STATUS);
		editor.remove(MESSAGE_STATUS);
		editor.remove(MISSED_CALL_STATUS);
		editor.remove(RECEIVED_CALL_STATUS);
		editor.remove(IS_WIFI_ONLY);
		editor.remove(IS_WIFI_OR_DATA);
		editor.remove(CONTACTS);
		
		editor.commit();
	}
	
}
