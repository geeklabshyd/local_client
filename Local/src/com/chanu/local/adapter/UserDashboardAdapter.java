package com.chanu.local.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chanu.local.R;

public class UserDashboardAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private Activity activity;
	private Context context;

	// Keep all Images in array
	private Integer[] mThumbIds = { R.drawable.lawn_mower,
			R.drawable.snow_shovel, R.drawable.car, R.drawable.swimming_pool };
	
	private String[] mThumbNames={"MOW MY LAWN", "SHOVEL MY SNOW", "WASH MY CAR","CLEAN MY POOL"};
	
	public UserDashboardAdapter(Activity activity ) {
		this.activity = activity;
		this.inflater = LayoutInflater.from(activity);
		this.context = activity.getApplicationContext();
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}
	public String getString(int position){
		return mThumbNames[position];
	}

	@Override
	public long getItemId(int position) {
		return mThumbIds[position];
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.dashborad_list_item, null);

			holder.listImage = (ImageView) convertView.findViewById(R.id.listImage);
			holder.nameTextView = (TextView) convertView.findViewById(R.id.listItemName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.listImage.setImageResource(mThumbIds[position]);
		holder.listImage.setScaleType(ImageView.ScaleType.FIT_END);
		/*holder.listImage.setLayoutParams(new AbsListView.LayoutParams((int) context.getResources().getDimension(R.dimen.cell_width),
																	(int) context.getResources().getDimension(R.dimen.cell_height)));*/
		holder.nameTextView.setText(mThumbNames[position]);
		return convertView;
	}

	private static class ViewHolder {
		
		public ImageView listImage;
		TextView nameTextView;

	}

}
