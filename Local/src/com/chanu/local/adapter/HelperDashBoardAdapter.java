package com.chanu.local.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chanu.local.R;

public class HelperDashBoardAdapter extends BaseAdapter {

	
	private Integer[] mThumbIds = { R.drawable.lawn_mower,
			R.drawable.snow_shovel, R.drawable.car };
	
	private String[] mThumbNames={"OFFER MY HELP", "PROMOTE MY HELP", "HELPER JOBS"};

	private Context context;

	private LayoutInflater inflater;
	
	public HelperDashBoardAdapter(Context context ) {
		this.context = context;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return mThumbIds[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.dashborad_list_item, null);

			holder.listImage = (ImageView) convertView.findViewById(R.id.listImage);
			holder.nameTextView = (TextView) convertView.findViewById(R.id.listItemName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.listImage.setImageResource(mThumbIds[position]);
		holder.listImage.setScaleType(ImageView.ScaleType.FIT_END);
		holder.nameTextView.setText(mThumbNames[position]);
		return convertView;
	}

	private static class ViewHolder {
		
		public ImageView listImage;
		TextView nameTextView;

	}

}
