package com.chanu.local.adapter;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chanu.local.R;
import com.chanu.local.sqlite.HelperService;

public class CommentsAdaptor extends BaseAdapter {

	private List<HelperService> helperServices = new ArrayList<HelperService>();
	private HelperService helperService;
	private LayoutInflater inflater;
	private int selectedPosition = 0;

	public List<HelperService> getHelperServices() {
		return helperServices;
	}

	public void setHelperServices(List<HelperService> helperServices) {
		this.helperServices = helperServices;
		notifyDataSetChanged();
	}

	public void clear() {
		helperServices.clear();
	}

	/*
	 * public CommentsAdaptor(Activity activity, List<HelperService>
	 * helperServices) { this.helperServices.clear(); this.helperServices =
	 * helperServices; this.activity = activity; this.inflater =
	 * LayoutInflater.from(activity); }
	 */
	public HelperService getSelectedHelper() {
		helperService = helperServices.get(selectedPosition);
		return helperService;

	}

	@Override
	public int getCount() {
		return helperServices.size();
	}

	@Override
	public Object getItem(int position) {
		return helperServices.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		helperService = helperServices.get(position);
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.custom_comment, null);

			holder.commentTextView = (TextView) convertView.findViewById(R.id.comment_textView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.commentTextView.setText("comments");
		return convertView;
	}

	private static class ViewHolder {

		TextView commentTextView;

	}

}
