package com.chanu.local.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chanu.local.sqlite.DatabaseHandler;
import com.chanu.local.sqlite.HelperService;
import com.chanu.local.R;

public class HelpersListAdapter extends BaseAdapter {

	private List<HelperService> helperServices = new ArrayList<HelperService>();
	private Activity activity;
	private HelperService helperService;
	private LayoutInflater inflater;
	private Context context;
	private String serviceType;
	private String date;
	private DatabaseHandler db;
	private int selectedPosition =0;

	public List<HelperService> getHelperServices() {
		return helperServices;
	}

	public void setHelperServices(List<HelperService> helperServices) {
		this.helperServices = helperServices;
		notifyDataSetChanged();
	}

	public void clear() {
		helperServices.clear();
	}

	public HelpersListAdapter(Activity activity,
			List<HelperService> helperServices) {
		this.helperServices.clear();
		this.helperServices = helperServices;
		this.activity = activity;
		this.inflater = LayoutInflater.from(activity);
	}
	public HelperService getSelectedHelper(){
		helperService = helperServices.get(selectedPosition);
		return helperService;
		
	}

	@Override
	public int getCount() {
		return helperServices.size();
	}

	@Override
	public Object getItem(int position) {
		return helperServices.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		helperService = helperServices.get(position);
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.helper_list_item, null);
			holder.helperName = (RadioButton) convertView.findViewById(R.id.helperName);
			holder.cost = (TextView) convertView.findViewById(R.id.costOfService);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.helperName.setText("" + helperService.getFirstName() + " " + helperService.getLastName());
		holder.cost.setText("" + helperService.getCost());
		
		holder.helperName.setChecked(position == selectedPosition);
		holder.helperName.setTag(position);
		holder.helperName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = (Integer)view.getTag();
                System.out.println(selectedPosition);
//                notifyDataSetChanged();
            }
        });
		return convertView;
	}
	private static class ViewHolder {
		public RadioButton helperName;
		TextView cost;
	}
}
